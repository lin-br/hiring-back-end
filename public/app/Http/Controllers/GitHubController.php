<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class GitHubController extends Controller
{
    private $requirements = array();
    private $responseJson = array();
    private $httpClient = null;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    public function getUser($user)
    {
        $bodyJson = $this->httpClient->get('https://api.github.com/users/' . $user)->getBody();
        $bodyArray = json_decode($bodyJson, true);

        $this->requirements = array("id","login","name","avatar_url","html_url");

        for ($counterX = 0; $counterX < count($this->requirements); $counterX++){
            foreach ($bodyArray as $key => $value){
                if ($this->requirements[$counterX] === $key) {
                    $this->responseJson[$key] = $value;
                }
            }
        }

        return response()->json($this->responseJson);
    }

    public function getUserRepos($user)
    {
        $bodyJson = $this->httpClient->get('https://api.github.com/users/' . $user . '/repos')->getBody();
        $bodyArray = json_decode($bodyJson, true);

        $this->requirements = array("id","name","description","html_url");

        for ($counterX = 0; $counterX < count($this->requirements); $counterX++){
            for ($counterY = 0; $counterY < count($bodyArray); $counterY++){
                foreach ($bodyArray[$counterY] as $key => $value){
                    if ($this->requirements[$counterX] === $key) {
                        $this->responseJson[$counterY][$key] = $value;
                    }
                }
            }
        }

        return response()->json($this->responseJson);
    }
}
