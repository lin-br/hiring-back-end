<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GithubUserTest extends TestCase
{
    private $user = 'lin-br';

    public function testReposJsonResponse()
    {
        $responseJson = $this->json('GET', '/github/users/' . $this->user . '/repos');
        $responseJson
            ->assertStatus(200)
            ->assertJsonStructure([['id','name','description','html_url']])
            ->assertJsonMissing(['login']);
    }

    public function testJsonResponse()
    {
        $responseJson = $this->json('GET', '/github/users/' . $this->user);
        $responseJson
            ->assertStatus(200)
            ->assertJsonStructure(['id','login','name','avatar_url','html_url'])
            ->assertJsonMissing(['description']);
    }
}
