<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/github/users/{user}', ['middleware' => 'cors', 'as' => 'github/users', 'uses' => 'GitHubController@getUser']);
Route::get('/github/users/{user}/repos', ['middleware' => 'cors', 'as' => 'github/users/repos/{user}', 'uses' => 'GitHubController@getUserRepos']);
