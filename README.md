# Test Hiring Creditoo

## Steps

- Download the repository
- Access the hiring-back-end directory with your favorite terminal.
- Run the command:
```
docker-compose up -d
```
- Access the public directory and run the command:
```
docker run -it --rm -v $(pwd):/app/ composer install
```
or, if you have composer installed:
```
composer install
```
- Access the **/tests/** directory and then the **/Feature/** directory.
- Change the variable 
```
private $user = 'lib-br';
```
From the **GithubUserTest.php** file to a valid **GitHub** platform user.
- Return to the **/hiring-back-end/public/** directory and run the command:
```
docker run -it --rm -v $(pwd):/app/ composer ./vendor/bin/phpunit
```
